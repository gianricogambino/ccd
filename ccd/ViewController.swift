//
//  ViewController.swift
//  ccd
//
//  Created by Gianrico on 04/12/15.
//  Copyright © 2015 REKServizi. All rights reserved.
//

import UIKit

// avendo in serito il pickerview abbiamo inserito anche i protocolli relativi necessari al suo funzionamento e più sotto  le funzioni obbligatorie

class ViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        //setto l'etichetta a invisibile fintanto che il pulsante non viene premuto e allora tornerà a 1
        //avviene nella funzione mioBottonePremuto()
        
        miaEtichetta.alpha = 0
        
        //dati della pickerview
        listaAnni.dataSource = self
        listaAnni.delegate = self
        
        //chiama la funziona creaListaAnni che valorizza l'array con l'elenco degli anni
        creaListaAnni()
        
        //setta il valore di default al primo valore dell'array listaAnniDati (2015)
        etichettaListaAnni.text = listaAnniDati[0]
        etichettaListaAnni.alpha = 0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //creazione mia etichetta fatta con interfaccia grafica in main.storyboard in cui apparirà il conto dei giorni mancanti a Natale
    @IBOutlet weak var miaEtichetta: UILabel!
    
    //creazione in storyboard di un pickerview per elencare i nomi degli anni
    @IBOutlet weak var etichettaListaAnni: UILabel!
    
    @IBOutlet weak var listaAnni: UIPickerView!
    
    //crea l'array per la lista anni e lo popola con il 2010
    var listaAnniDati: [String] = ["Seleziona un anno e clicca"]
    
    
    //////////////////////////////// CREALISTAANNI()  ////////////////////////////////////////////////
    //
    //funzione per creare gli anni che vogliamo in questo caso andremo dal 2010 già inserito al 2019
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    func creaListaAnni ()
    {
        for (var indice = 2010; indice < 2020; indice++)
        {
            //convertiamo in stringa l'indice
            let indiceConvertito: String = (indice as NSInteger).description
            //inseriamo l'indice  convertito dentro l'array di stringhe
            listaAnniDati.append(indiceConvertito)
        }
    }
    
    
    //////////////////////////////// FUNC PICKERVIEW  ////////////////////////////////////////////////
    //
    //funzioni standard senza le quali in alto vicino alla dichiarazione di classe restituirebbe un errore
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    //questa dichiarazione tre volte di una funzione con lo stesso nome è un override o qualcosa del genere?
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        print(listaAnniDati.count)
        return listaAnniDati.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listaAnniDati[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        etichettaListaAnni.text = listaAnniDati[row]
    }
    

    //////////////////////////////// FUNC DAYTOXMAS()  ////////////////////////////////////////////////
    //
    //
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    // COSTANTI comuni al calcolo del numero di giorni tra oggi e Natale 2015 e 2016
    
    let oggi = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 0, toDate: NSDate(), options: [])
    let nataleComponents = NSDateComponents()
    let components: NSCalendarUnit = .Day

    var oggiPrimaODopo = true
    
    func daysToXmas () -> Int{
        
        if etichettaListaAnni.text == listaAnniDati[0]
        {
            nataleComponents.year = 2015
        }
        else
        {
            //come mai serve il punto esclamativo per la coversione?
            let etichettaConvertita: Int = (etichettaListaAnni.text! as NSString).integerValue
            nataleComponents.year = etichettaConvertita
            

        }
        
        nataleComponents.month = 12
        nataleComponents.day = 25
        let natale = NSCalendar.currentCalendar().dateFromComponents(nataleComponents)!
        
        
        let diff = NSCalendar.currentCalendar().components(components, fromDate: oggi!, toDate: natale, options: [])
        return diff.day
    }
    
    
    //////////////////////////////// FUNC MIOBOTTONEPREMUTO  ////////////////////////////////////////////////
    //
    //           creazione funzione di pressione bottone creata da interfaccia main.storyboard
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    @IBAction func mioBottonePremuto(sender: AnyObject)
        
    {
        //dopoNatale()
        miaEtichetta.alpha = 1
        
        if daysToXmas()>0
        {
            miaEtichetta.text = "Mancano \(daysToXmas()) giorni a Natale!"
        }
        else
        {
            miaEtichetta.text = "Sono passati \(daysToXmas() * -1) giorni da Natale."
        }
    }
    


}

